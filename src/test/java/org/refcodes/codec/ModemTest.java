// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;
import org.refcodes.io.LoopbackShortsReceiver;
import org.refcodes.io.LoopbackShortsTransmitter;
import org.refcodes.io.ShortArrayReceiver;
import org.refcodes.runtime.SystemProperty;

public class ModemTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// @Ignore
	@Test
	public void testModem1() throws IOException, InterruptedException {
		final ModemMetricsImpl theModemMetrics = new ModemMetricsImpl( SampleRate.STEREO, ModulationFormat.SHORT, ChannelSelector.MONO, ModemMode.SOFT_MODEM_4, FrequencyThreshold.THRESHOLD_20 );
		final List<Short> theEncoded = new ArrayList<>();
		final ModemEncoderImpl theEncoder = new ModemEncoderImpl( theModemMetrics, ( short[] aShorts, int aOffset, int aLength ) -> {
			// if ( SystemProperty.LOG_TESTS.isEnabled() ) System.out.println( Arrays.toString( aDatagrams ) );
			for ( int i = 0; i < aLength; i++ ) {
				theEncoded.add( aShorts[i + aOffset] );
			}
		} );
		theEncoder.transmitBytes( Text.ARECIBO_MESSAGE.toString().getBytes() );
		final ModemDecoderImpl theDecoder = new ModemDecoderImpl( theModemMetrics, new ShortArrayReceiver( theEncoded ) );
		final List<Byte> theDecoded = new ArrayList<>();
		while ( theDecoder.hasAvailable() ) {
			// if ( theDecoder.hasAvailable() ) theDecoded.add( theDecoder.accept() );
			theDecoded.add( theDecoder.receiveByte() );
		}
		final String theMessage = new String( toPrimitiveType( theDecoded.toArray( new Byte[theDecoded.size()] ) ) );
		final String[] theLines = theMessage.split( SystemProperty.LINE_SEPARATOR.getValue() );
		for ( String eLine : theLines ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eLine );
			}
		}
		assertEquals( Text.ARECIBO_MESSAGE.toString(), theMessage );
	}

	@Test
	public void testModem2() throws IOException, InterruptedException {
		final ModemMetricsImpl theModemMetrics = new ModemMetricsImpl( SampleRate.STEREO, ModulationFormat.SHORT, ChannelSelector.MONO, ModemMode.SOFT_MODEM_4, FrequencyThreshold.THRESHOLD_20 );
		final String theMessage = Text.ARECIBO_MESSAGE.toString();
		final LoopbackShortsReceiver theReceiver = new LoopbackShortsReceiver( 656532 );
		final LoopbackShortsTransmitter theSender = new LoopbackShortsTransmitter();
		theReceiver.open( theSender );
		theSender.open( theReceiver );
		final ModemEncoderImpl theEncoder = new ModemEncoderImpl( theModemMetrics, theSender );
		theEncoder.transmitBytes( theMessage.getBytes() );
		final ModemDecoderImpl theDecoder = new ModemDecoderImpl( theModemMetrics, theReceiver );
		final List<Byte> theDecoded = new ArrayList<>();
		while ( theDecoder.hasAvailable() ) {
			// if ( theDecoder.hasAvailable() ) theDecoded.add( theDecoder.accept() );
			theDecoded.add( theDecoder.receiveByte() );
		}
		final String theReceived = new String( toPrimitiveType( theDecoded.toArray( new Byte[theDecoded.size()] ) ) );
		final String[] theLines = theReceived.split( SystemProperty.LINE_SEPARATOR.getValue() );
		for ( String eLine : theLines ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eLine );
			}
		}
		assertEquals( theMessage, theMessage );
	}

	@Test
	public void testModem3() throws IOException, InterruptedException {
		final ModemMetricsImpl theModemMetrics = new ModemMetricsImpl( SampleRate.STEREO, ModulationFormat.SHORT, ChannelSelector.MONO, ModemMode.SOFT_MODEM_4, FrequencyThreshold.THRESHOLD_20 );
		final String theMessage = Text.ARECIBO_MESSAGE.toString();
		final LoopbackShortsReceiver theReceiver = new LoopbackShortsReceiver( 656532 );
		final LoopbackShortsTransmitter theSender = new LoopbackShortsTransmitter();
		theReceiver.open( theSender );
		theSender.open( theReceiver );
		final ModemEncoderImpl theEncoder = new ModemEncoderImpl( theModemMetrics, theSender );
		theEncoder.transmitBytes( theMessage.getBytes() );
		final ModemDecoderImpl theDecoder = new ModemDecoderImpl( theModemMetrics, theReceiver );
		byte[] theData = null;
		byte[] tmpData;
		byte[] eData = null;
		while ( theDecoder.hasAvailable() ) {
			eData = theDecoder.receiveAllBytes();
			if ( theData == null ) {
				theData = eData;
			}
			else {
				tmpData = new byte[theData.length + eData.length];
				System.arraycopy( theData, 0, tmpData, 0, theData.length );
				System.arraycopy( eData, 0, tmpData, theData.length, eData.length );
				theData = tmpData;
			}
		}
		final String[] theLines = new String( theData ).split( SystemProperty.LINE_SEPARATOR.getValue() );
		for ( String eLine : theLines ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eLine );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static byte[] toPrimitiveType( Byte[] aBytes ) {
		if ( aBytes == null ) {
			return null;
		}
		final byte[] thePrimitives = new byte[aBytes.length];
		for ( int i = 0; i < aBytes.length; i++ ) {
			thePrimitives[i] = aBytes[i].byteValue();
		}
		return thePrimitives;
	}
}
