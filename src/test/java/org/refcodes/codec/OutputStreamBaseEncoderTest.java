// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;

/**
 * The Class BaseOutputStreamEncoderTest.
 */
public class OutputStreamBaseEncoderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int WEAK_INTENSITY_LOOPS = 4096;
	private static final int MAX_DATA_LENGTH = 4096;

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAllRandomText() throws IOException {
		for ( BaseMetrics eCodec : BaseMetricsConfig.values() ) {
			runRandomTextTest( eCodec );
		}
	}

	@Test
	public void testAllRandomBytes() throws IOException {
		for ( BaseMetrics eCodec : BaseMetricsConfig.values() ) {
			runRandomBytesTest( eCodec );
		}
	}

	@Disabled("Just for debugging purposes")
	@Test
	public void debugBaseEncodeOutputStream1() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final BaseMetricsConfig theCodecMetrics = BaseMetricsConfig.BASE64;
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( theCodecMetrics );
		final byte[] theDecodedBytes = "Hello world!".getBytes();
		theBaseCodeBuilder.withDecodedData( theDecodedBytes );
		final String theEncodedText = theBaseCodeBuilder.getEncodedText();
		final BaseEncoder theEncodeSender = new BaseEncoder( theOutputStream ).withBaseMetrics( theCodecMetrics );
		theEncodeSender.transmitBytes( theDecodedBytes );
		theEncodeSender.close();
		final String theResult = new String( theOutputStream.toByteArray() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Expecting <" + theResult + "> to be <" + theEncodedText + "> ..." );
		}
		assertEquals( theEncodedText, theResult );
	}

	@Disabled("Just for debugging purposes")
	@Test
	public void debugBaseEncodeOutputStream2() throws IOException {
		final ByteArrayOutputStream theOutputStream = new ByteArrayOutputStream();
		final BaseMetricsConfig theCodecMetrics = BaseMetricsConfig.BASE64;
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( theCodecMetrics );
		final byte[] theDecodedBytes = "Hello world!!".getBytes();
		theBaseCodeBuilder.withDecodedData( theDecodedBytes );
		final String theEncodedText = theBaseCodeBuilder.getEncodedText();
		final BaseEncoder theEncodeSender = new BaseEncoder( theOutputStream ).withBaseMetrics( theCodecMetrics );
		theEncodeSender.transmitBytes( theDecodedBytes );
		theEncodeSender.close();
		final String theResult = new String( theOutputStream.toByteArray() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Expecting <" + theResult + "> to be <" + theEncodedText + "> ..." );
		}
		assertEquals( theEncodedText, theResult );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toRandomText( int aLength ) {
		return RandomTextGenerartor.asString( aLength, RandomTextMode.ASCII );
	}

	private void runRandomTextTest( BaseMetrics aBaseMetrics ) throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Running random text tests for base <" + aBaseMetrics.getNumberBase() + "> ..." );
		}
		ByteArrayOutputStream eOutputStream;
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		String eReferenceEncodedText;
		String eEncodedText;
		byte[] eInputBytes;
		byte[] eDecodedBytes;
		String eRndText;
		BaseEncoder eBaseEncodeOutputStream;
		for ( int i = 0; i < WEAK_INTENSITY_LOOPS; i++ ) {
			eOutputStream = new ByteArrayOutputStream();
			eBaseEncodeOutputStream = new BaseEncoder( eOutputStream ).withBaseMetrics( aBaseMetrics );
			eRndText = toRandomText( i % MAX_DATA_LENGTH );
			eInputBytes = eRndText.getBytes();
			eEncodedText = theBaseCodeBuilder.withDecodedData( eInputBytes ).getEncodedText();
			eBaseEncodeOutputStream.transmitBytes( eInputBytes );
			eBaseEncodeOutputStream.close();
			final String theResult = new String( eOutputStream.toByteArray() );
			assertEquals( eEncodedText, theResult );
			// if (IS_LOG_TESTS_ENABLED) System.out.println( theRndText + ": Reference := " + eReferenceEncoding
			// + " --> REFCODES := " + eEncodedText );
			eDecodedBytes = theBaseCodeBuilder.withEncodedText( eEncodedText ).getDecodedData();
			assertArrayEquals( eInputBytes, eDecodedBytes );
			if ( aBaseMetrics == BaseMetricsConfig.BASE64 ) {
				eReferenceEncodedText = Base64.getEncoder().encodeToString( eInputBytes );
				assertEquals( eReferenceEncodedText, eEncodedText );
			}
		}
	}

	private void runRandomBytesTest( BaseMetrics aBaseMetrics ) throws IOException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Running random bytes tests for base <" + aBaseMetrics.getNumberBase() + "> ..." );
		}
		ByteArrayOutputStream eOutputStream;
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		String eReferenceEncodedText;
		String eEncodedText;
		int eMod;
		byte[] eInputBytes;
		byte[] eDecodedBytes;
		BaseEncoder eBaseEncodeOutputStream;
		for ( int i = 0; i < WEAK_INTENSITY_LOOPS; i++ ) {
			eOutputStream = new ByteArrayOutputStream();
			eBaseEncodeOutputStream = new BaseEncoder( eOutputStream ).withBaseMetrics( aBaseMetrics );
			eMod = i % MAX_DATA_LENGTH;
			eInputBytes = new byte[eMod];
			RND.nextBytes( eInputBytes );
			eEncodedText = theBaseCodeBuilder.withDecodedData( eInputBytes ).getEncodedText();
			eBaseEncodeOutputStream.transmitBytes( eInputBytes );
			eBaseEncodeOutputStream.close();
			final String theResult = new String( eOutputStream.toByteArray() );
			assertEquals( eEncodedText, theResult );
			// if (IS_LOG_TESTS_ENABLED) System.out.println( new VerboseTextBuilderImpl().withElements( eBytes )
			// + ": Reference := " + eReferenceEncoding + " --> REFCODES := " +
			// eEncodedText );
			eDecodedBytes = theBaseCodeBuilder.withEncodedText( eEncodedText ).getDecodedData();
			assertArrayEquals( eInputBytes, eDecodedBytes );
			if ( aBaseMetrics == BaseMetricsConfig.BASE64 ) {
				eReferenceEncodedText = Base64.getEncoder().encodeToString( eInputBytes );
				assertEquals( eReferenceEncodedText, eEncodedText );
			}
		}
	}
}
