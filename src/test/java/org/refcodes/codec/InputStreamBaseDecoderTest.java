// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;

public class InputStreamBaseDecoderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int WEAK_INTENSITY_LOOPS = 4096;
	private static final int MAX_DATA_LENGTH = 4096;

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAllRandomText() throws IOException, InterruptedException {
		for ( BaseMetrics eCodec : BaseMetricsConfig.values() ) {
			runRandomTextTest( eCodec );
		}
	}

	@Test
	public void testAllRandomBytes() throws IOException, InterruptedException {
		for ( BaseMetrics eCodec : BaseMetricsConfig.values() ) {
			runRandomBytesTest( eCodec );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("Just for debugging purposes")
	@Test
	public void debugBaseDecodeInputStreamTest1() throws IOException, InterruptedException {
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( BaseMetricsConfig.BINARY );
		final String theInputText = "Hello World!?";
		final byte[] theInputBytes = theInputText.getBytes();
		final String theEncodedText = theBaseCodeBuilder.withDecodedData( theInputBytes ).getEncodedText();
		final ByteArrayInputStream theInputStream = new ByteArrayInputStream( theEncodedText.getBytes() );
		final BaseDecoder theDecodeReceiver = new BaseDecoder( theInputStream ).withBaseMetrics( BaseMetricsConfig.BINARY );
		final List<Byte> theDecodedBytes = new ArrayList<>();
		byte[] eDecodedBytes;
		while ( theDecodeReceiver.hasAvailable() ) {
			eDecodedBytes = theDecodeReceiver.receiveAllBytes();
			for ( Byte eByte : eDecodedBytes ) {
				theDecodedBytes.add( eByte );
			}
		}
		theDecodeReceiver.close();
		// if ( SystemProperty.LOG_TESTS.isEnabled() ) System.out.println( "Input   := " + Arrays.toString( theInputBytes ) );
		// if ( SystemProperty.LOG_TESTS.isEnabled() ) System.out.println( "Decoded := " + Arrays.toString( theDecodedBytes.toArray() ) );
		final String theDecodedText = new String( toPrimitiveType( theDecodedBytes.toArray( new Byte[theDecodedBytes.size()] ) ) );
		// if ( SystemProperty.LOG_TESTS.isEnabled() ) System.out.println( "Expecting <" + theDecodedText + "> to be <" + theInputText + "> ..." );
		assertEquals( theInputText, theDecodedText );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void runRandomTextTest( BaseMetrics aBaseMetrics ) throws IOException, InterruptedException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Running random text tests for base <" + aBaseMetrics.getNumberBase() + "> ..." );
		}
		ByteArrayInputStream eInputStream;
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		String eDecodedText;
		String eEncodedText;
		byte[] eInputBytes;
		String eRndText;
		List<Byte> eDecodedBytes;
		byte[] eDecodedBlock;
		BaseDecoder eDecodeReceiver;
		for ( int i = 0; i < WEAK_INTENSITY_LOOPS; i++ ) {
			eRndText = toRandomText( i % MAX_DATA_LENGTH );
			eInputBytes = eRndText.getBytes();
			eEncodedText = theBaseCodeBuilder.withDecodedData( eInputBytes ).getEncodedText();
			eInputStream = new ByteArrayInputStream( eEncodedText.getBytes() );
			eDecodeReceiver = new BaseDecoder( eInputStream ).withBaseMetrics( aBaseMetrics );
			eDecodedBytes = new ArrayList<>();
			while ( eDecodeReceiver.hasAvailable() ) {
				eDecodedBlock = eDecodeReceiver.receiveAllBytes();
				for ( Byte eByte : eDecodedBlock ) {
					eDecodedBytes.add( eByte );
				}
			}
			eDecodeReceiver.close();
			eDecodedText = new String( toPrimitiveType( eDecodedBytes.toArray( new Byte[eDecodedBytes.size()] ) ) );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Expecting <" + eDecodedText + "> to be <" + eRndText + "> ..." );
			}
			assertEquals( eRndText, eDecodedText );
		}
	}

	private void runRandomBytesTest( BaseMetrics aBaseMetrics ) throws IOException, InterruptedException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Running random text tests for base <" + aBaseMetrics.getNumberBase() + "> ..." );
		}
		ByteArrayInputStream eInputStream;
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		String eEncodedText;
		byte[] eInputBytes;
		List<Byte> eDecodedByteList;
		byte[] eDecodedBlock;
		byte[] eDecodedBytes;
		int eMod;
		BaseDecoder eDecodeReceiver;
		for ( int i = 0; i < WEAK_INTENSITY_LOOPS; i++ ) {
			eMod = i % MAX_DATA_LENGTH;
			eInputBytes = new byte[eMod];
			RND.nextBytes( eInputBytes );
			eEncodedText = theBaseCodeBuilder.withDecodedData( eInputBytes ).getEncodedText();
			eInputStream = new ByteArrayInputStream( eEncodedText.getBytes() );
			eDecodeReceiver = new BaseDecoder( eInputStream ).withBaseMetrics( aBaseMetrics );
			eDecodedByteList = new ArrayList<>();
			while ( eDecodeReceiver.hasAvailable() ) {
				eDecodedBlock = eDecodeReceiver.receiveAllBytes();
				for ( Byte eByte : eDecodedBlock ) {
					eDecodedByteList.add( eByte );
				}
			}
			eDecodeReceiver.close();
			eDecodedBytes = toPrimitiveType( eDecodedByteList.toArray( new Byte[eDecodedByteList.size()] ) );
			// if ( SystemProperty.LOG_TESTS.isEnabled() ) System.out.println( "Input := " + Arrays.toString( eInputBytes ) );
			// if ( SystemProperty.LOG_TESTS.isEnabled() ) System.out.println( "Decoded := " + Arrays.toString( eDecodedBytes ) );
			assertArrayEquals( eInputBytes, eDecodedBytes );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static byte[] toPrimitiveType( Byte[] aBytes ) {
		if ( aBytes == null ) {
			return null;
		}
		final byte[] thePrimitives = new byte[aBytes.length];
		for ( int i = 0; i < aBytes.length; i++ ) {
			thePrimitives[i] = aBytes[i].byteValue();
		}
		return thePrimitives;
	}

	private String toRandomText( int aLength ) {
		return RandomTextGenerartor.asString( aLength, RandomTextMode.ASCII );
	}
}
