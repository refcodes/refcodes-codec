// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Base64;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.CharSet;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;

/**
 * The Class BaseBuilderTest.
 */
public class BaseBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int WEAK_INTENSITY_LOOPS = 10240;
	private static final int STRONG_INTENSITY_LOOPS = 150000;
	private static final int MAX_DATA_LENGTH = 8192;
	private static final char[] CHAR_SET = CharSet.ALPHANUMERIC.getCharSet();
	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	//	@Test
	//	public void testDiv() {
	//		for ( int i = 0; i < 1024; i++ ) {
	//			//			float a = i / 1024F;
	//			//			int b = (int) (a * 100F);
	//			//			float c = b / 100F;
	//			//			System.out.println( i + "= " + c );
	//			float result = ((int) (i / 1024F * 100F)) / 100F;
	//			System.out.println( i + "= " + result );
	//		}
	//
	//	}

	@Test
	public void testAllRandomText() {
		boolean isTestSideEffectFreeCall = true;
		for ( BaseMetrics eCodec : BaseMetricsConfig.values() ) {
			runRandomTextTest( eCodec, isTestSideEffectFreeCall );
			isTestSideEffectFreeCall = !isTestSideEffectFreeCall;
		}
	}

	@Test
	public void testAllRandomBytes() {
		boolean isTestSideEffectFreeCall = true;
		for ( BaseMetrics eCodec : BaseMetricsConfig.values() ) {
			runRandomBytesTest( eCodec, isTestSideEffectFreeCall );
			isTestSideEffectFreeCall = !isTestSideEffectFreeCall;
		}
	}

	@Test
	public void testBlockSize() {
		// BaseMetrics eCodec = BaseMetricsConfig.BASE64;
		for ( BaseMetrics eCodec : BaseMetricsConfig.values() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eCodec.toString() );
			}
			final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( eCodec );
			String eEncoded;
			int ePaddingNumber;
			for ( int i = 1; i < 64; i++ ) {
				eEncoded = theBaseCodeBuilder.toEncodedText( RandomTextGenerartor.asString( i, RandomTextMode.ALPHANUMERIC ) );
				final int eCount = count( eEncoded, eCodec.getPaddingChar() );
				ePaddingNumber = eCodec.toPaddingCharsNumber( i );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "[" + ( eCount != ePaddingNumber ? "X" : "✓" ) + "] " + ePaddingNumber + " ? " + eCount + ": " + eEncoded + " (" + i + ")" );
				}
				assertEquals( eCount, ePaddingNumber );
			}
		}
	}

	@Disabled("Just for debugging purposes")
	@Test
	public void debugeEdgeCase() {
		final BaseMetrics theMetrics = BaseMetricsConfig.ENCODED_AS_NUMBER;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theMetrics );
		}
		final BaseBuilder theBuilder = new BaseBuilder().withBaseMetrics( theMetrics );
		final String theEncoded = theBuilder.toEncodedText( "Hallo welt! 1234567890 ABC".getBytes() );
		final byte[] theDecoded = theBuilder.toDecodedData( theEncoded );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theEncoded );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new String( theDecoded ) );
		}
	}

	@Test
	public void debugEncodedAsNumberCodec() {
		runRandomTextTest( BaseMetricsConfig.ENCODED_AS_NUMBER, false );
	}

	@Test
	public void debugBase64Codec() {
		final BaseMetrics theBase64 = new BaseMetricsImpl( 64, CharSet.BASE64.getCharSet() );
		runRandomTextTest( theBase64, true );
	}

	@Disabled("Just for debugging purposes")
	@Test
	public void debugGetBytesFromText() {
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( BaseMetricsConfig.BASE64 );
		theBaseCodeBuilder.withDecodedData( "Hello world!".getBytes() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBaseCodeBuilder.getEncodedText() );
		}
	}

	@Disabled("Just for throughput measurement")
	@Test
	public void testEncodeRefcodes() {
		byte[] eBytes;
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( BaseMetricsConfig.BASE64 );
		for ( int i = 0; i < STRONG_INTENSITY_LOOPS; i++ ) {
			final String theRndText = toRandom( i % MAX_DATA_LENGTH );
			eBytes = theRndText.getBytes();
			theBaseCodeBuilder.withDecodedData( eBytes ).getEncodedText();
		}
	}

	@Disabled("Just for throughput measurement")
	@Test
	public void testEncodeJava() {
		byte[] eBytes;
		for ( int i = 0; i < STRONG_INTENSITY_LOOPS; i++ ) {
			final String theRndText = toRandom( i % MAX_DATA_LENGTH );
			eBytes = theRndText.getBytes();
			Base64.getEncoder().encodeToString( eBytes );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void runRandomTextTest( BaseMetrics aBaseMetrics, boolean isWithSideEffectFreeShortcut ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Running random text tests for base <" + ( aBaseMetrics.getNumberBase() < 10 ? "0" : "" ) + aBaseMetrics.getNumberBase() + "> (" + ( isWithSideEffectFreeShortcut ? "*" : "?" ) + ") ..." );
		}
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		String eReferenceEncodedText;
		String eEncodedText;
		byte[] eInputBytes;
		byte[] eDecodedBytes;
		String eRndText;
		for ( int i = 0; i < WEAK_INTENSITY_LOOPS; i++ ) {
			eRndText = toRandom( i % MAX_DATA_LENGTH );
			eInputBytes = eRndText.getBytes();
			if ( !isWithSideEffectFreeShortcut ) {
				eEncodedText = theBaseCodeBuilder.withDecodedData( eInputBytes ).getEncodedText();
			}
			else {
				eEncodedText = theBaseCodeBuilder.toEncodedText( eInputBytes );
			}
			// if (IS_LOG_TESTS_ENABLED) System.out.println( theRndText + ": Reference := " + eReferenceEncoding
			// + " --> REFCODES := " + eEncodedText );
			eDecodedBytes = theBaseCodeBuilder.withEncodedText( eEncodedText ).getDecodedData();
			assertArrayEquals( eInputBytes, eDecodedBytes );
			if ( aBaseMetrics == BaseMetricsConfig.BASE64 ) {
				eReferenceEncodedText = Base64.getEncoder().encodeToString( eInputBytes );
				assertEquals( eReferenceEncodedText, eEncodedText );
			}
		}
	}

	private void runRandomBytesTest( BaseMetrics aBaseMetrics, boolean isWithSideEffectFreeShortcut ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Running random bytes tests for base <" + ( aBaseMetrics.getNumberBase() < 10 ? "0" : "" ) + aBaseMetrics.getNumberBase() + "> (" + ( isWithSideEffectFreeShortcut ? "*" : "?" ) + ") ..." );
		}
		final BaseBuilder theBaseCodeBuilder = new BaseBuilder().withBaseMetrics( aBaseMetrics );
		String eReferenceEncodedText;
		String eEncodedText;
		int eMod;
		byte[] eInputBytes;
		byte[] eDecodedBytes;
		for ( int i = 0; i < WEAK_INTENSITY_LOOPS; i++ ) {
			eMod = i % MAX_DATA_LENGTH;
			eInputBytes = new byte[eMod];
			RND.nextBytes( eInputBytes );
			if ( !isWithSideEffectFreeShortcut ) {
				eEncodedText = theBaseCodeBuilder.withDecodedData( eInputBytes ).getEncodedText();
			}
			else {
				eEncodedText = theBaseCodeBuilder.toEncodedText( eInputBytes );
			}
			// if (IS_LOG_TESTS_ENABLED) System.out.println( new VerboseTextBuilderImpl().withElements( eBytes )
			// + ": Reference := " + eReferenceEncoding + " --> REFCODES := " +
			// eEncodedText );
			eDecodedBytes = theBaseCodeBuilder.withEncodedText( eEncodedText ).getDecodedData();
			assertArrayEquals( eInputBytes, eDecodedBytes );
			if ( aBaseMetrics == BaseMetricsConfig.BASE64 ) {
				eReferenceEncodedText = Base64.getEncoder().encodeToString( eInputBytes );
				assertEquals( eReferenceEncodedText, eEncodedText );
			}
		}
	}

	private String toRandom( int aColumnWidth ) {
		final int theBound = CHAR_SET.length;
		final char[] theRandom = new char[aColumnWidth];
		for ( int i = 0; i < aColumnWidth; i++ ) {
			theRandom[i] = CHAR_SET[RND.nextInt( theBound )];
		}
		return new String( theRandom );
	}

	private int count( String aString, char aChar ) {
		int theCount;
		int eIndexOf;
		theCount = 0;
		eIndexOf = aString.indexOf( aChar );
		while ( eIndexOf != -1 ) {
			aString = aString.substring( eIndexOf + 1 );
			theCount++;
			eIndexOf = aString.indexOf( aChar );
		}
		return theCount;
	}
}
