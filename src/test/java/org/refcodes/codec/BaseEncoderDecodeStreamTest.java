// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.MemoryUnit;
import org.refcodes.exception.Trap;
import org.refcodes.io.FileUtility;
import org.refcodes.io.InputStreamByteReceiver;
import org.refcodes.runtime.SystemProperty;

public class BaseEncoderDecodeStreamTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private final Random theRnd = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testBaseEncoderDecodeStream() throws IOException {
		final MemoryUnit[] theMemUnits = new MemoryUnit[] { MemoryUnit.BYTE, MemoryUnit.KILOBYTE, MemoryUnit.MEGABYTE };
		for ( BaseMetricsConfig eCodec : BaseMetricsConfig.values() ) {
			for ( MemoryUnit eMemUnit : theMemUnits ) {
				for ( int i = 1; i < 4; i++ ) {
					if ( eCodec.getNumberBase() >= 16 || eMemUnit != MemoryUnit.MEGABYTE ) { // Skipping the small ones as of long test duration!
						long eSize = eMemUnit.toBytes( i ).longValue();
						if ( eMemUnit != MemoryUnit.BYTE ) {
							eSize += theRnd.nextInt( 32 ); // Make the length vary a little (test the padding)
						}
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Testing file size <" + eSize + "> for codec <" + eCodec + "> ..." );
						}
						final File theBigFile = FileUtility.createRandomTempFile( eSize );
						final File theEncodedFile = FileUtility.createTempFile();
						final File theDecodedFile = FileUtility.createTempFile();
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Big file := " + theBigFile.getAbsolutePath() );
							System.out.println( "Encoded file := " + theEncodedFile.getAbsolutePath() );
							System.out.println( "Decoded file := " + theDecodedFile.getAbsolutePath() );
						}
						try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); OutputStream theEncoderOutputStream = new BaseEncoderOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), eCodec ) ) {
							theBigInputStream.transferTo( theEncoderOutputStream ); // Make sure "theEncoderOutputStream" is closed as the padding bytes finally are appended! 
						}
						try ( InputStream theDecoderInputStream = new BaseDecoderInputStream( new BufferedInputStream( new FileInputStream( theEncodedFile ) ), eCodec ); OutputStream theDecodedOutputStream = new BufferedOutputStream( new FileOutputStream( theDecodedFile ) ) ) {
							theDecoderInputStream.transferTo( theDecodedOutputStream );
						}
						if ( !FileUtility.isEqual( theBigFile, theDecodedFile ) ) {
							fail( "The files are not(!) equal!" );
						}
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println();
						}
					}
				}
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Done!" );
		}
	}

	@Test
	public void testBaseEncoderDecodeStreamSkipWhiteSpaces() throws IOException {
		final MemoryUnit[] theMemUnits = new MemoryUnit[] { MemoryUnit.BYTE, MemoryUnit.KILOBYTE };
		for ( BaseMetricsConfig eCodec : BaseMetricsConfig.values() ) {
			for ( MemoryUnit eMemUnit : theMemUnits ) {
				for ( int i = 1; i < 4; i++ ) {
					if ( eCodec.getNumberBase() >= 16 ) {
						long eSize = eMemUnit.toBytes( i ).longValue();
						if ( eMemUnit != MemoryUnit.BYTE ) {
							eSize += theRnd.nextInt( 32 ); // Make the length vary a little (test the padding)
						}
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Testing file size <" + eSize + "> for codec <" + eCodec + "> ..." );
						}
						final File theBigFile = FileUtility.createRandomTempFile( eSize );
						final File theEncodedFile = FileUtility.createTempFile();
						final File theDecodedFile = FileUtility.createTempFile();
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Big file := " + theBigFile.getAbsolutePath() );
							System.out.println( "Encoded file := " + theEncodedFile.getAbsolutePath() );
							System.out.println( "Decoded file := " + theDecodedFile.getAbsolutePath() );
						}
						try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); OutputStream theEncoderOutputStream = new BaseEncoderOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), eCodec ) ) {
							theBigInputStream.transferTo( theEncoderOutputStream ); // Make sure "theEncoderOutputStream" is closed as the padding bytes finally are appended! 
						}
						try ( InputStream theDecoderInputStream = new BaseDecoderInputStream( new BufferedInputStream( new FileInputStream( theEncodedFile ) ), eCodec, true ); OutputStream theDecodedOutputStream = new BufferedOutputStream( new FileOutputStream( theDecodedFile ) ) ) {
							theDecoderInputStream.transferTo( theDecodedOutputStream );
						}
						if ( !FileUtility.isEqual( theBigFile, theDecodedFile ) ) {
							fail( "The files are not(!) equal!" );
						}
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println();
						}
					}
				}
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Done!" );
		}
	}

	@Test
	@Disabled("Just for debugging edge cases")
	public void testEdgeCase() throws IOException {
		final File theBigFile = FileUtility.createRandomTempFile( "temp_big", MemoryUnit.BYTE.toBytes( 16 ).longValue(), false );
		final File theEncodedFile = FileUtility.createTempFile( "temp_encoded", false );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Big file := " + theBigFile.getAbsolutePath() );
			System.out.println( "Encoded file := " + theEncodedFile.getAbsolutePath() );
		}
		try ( InputStream theBigInputStream = new BufferedInputStream( new FileInputStream( theBigFile ) ); OutputStream theEncoderOutputStream = new BaseEncoderOutputStream( new BufferedOutputStream( new FileOutputStream( theEncodedFile ) ), BaseMetricsConfig.BASE64 ); InputStream theEncodedInputStream = new BufferedInputStream( new FileInputStream( theEncodedFile ) ); InputStream theEncodedInputStream2 = new BufferedInputStream( new FileInputStream( theEncodedFile ) ); ) {
			theBigInputStream.transferTo( theEncoderOutputStream );
		}
		try ( InputStream theEncodedInputStream = new BufferedInputStream( new FileInputStream( theEncodedFile ) ); InputStream theEncodedInputStream2 = new BufferedInputStream( new FileInputStream( theEncodedFile ) ); ) {
			final InputStreamByteReceiver theReceiver = new InputStreamByteReceiver( theEncodedInputStream );
			while ( theReceiver.hasAvailable() ) {
				final char eByte = (char) theReceiver.receiveByte();
				System.out.print( eByte );
			}
			System.out.println( "\n---" );
			while ( theEncodedInputStream2.available() != 0 ) {
				final char eByte = (char) theEncodedInputStream2.read();
				System.out.print( eByte );
			}
			System.out.println( "\n---" );
		}
		catch ( IOException e ) {
			e.printStackTrace();
			System.out.println( Trap.asMessage( e ) );
		}
	}
}
