module org.refcodes.codec {
	requires org.refcodes.data;
	requires org.refcodes.textual;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.io;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.numerical;
	requires org.refcodes.runtime;

	exports org.refcodes.codec;
}
