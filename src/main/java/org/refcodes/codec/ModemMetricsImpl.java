// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * The code has been INSPIRED by the following available informations out there:
 * -----------------------------------------------------------------------------
 * "https://en.wikipedia.org/wiki/XMODEM"
 * "http://www.java2s.com/Code/Java/Network-Protocol/JModemsimplecommunicationsprogram.htm"
 * "https://github.com/cytecbg/android-fskmodem"
 */

package org.refcodes.codec;

/**
 * The Class ModemMetricsImpl.
 */
public class ModemMetricsImpl implements ModemMetrics {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final SampleRate _sampleRate;
	private final ModulationFormat _modulationFormat;
	private final ChannelSelector _channelSelector;
	private final ModemMode _softModem;
	private FrequencyThreshold _frequencyThreshold;
	private final int _higherFrequencyUpperThreshold;
	private final int _lowerFrequencyUpperThreshold;
	private final int _samplesPerBit;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new modem metrics impl.
	 *
	 * @param aSampleRate the sample rate
	 * @param aModulationFormat the modulation format
	 * @param aChannelSelector the channel selector
	 * @param aModemModem the modem modem
	 * @param aFrequencyThreshold the frequency threshold
	 */
	public ModemMetricsImpl( SampleRate aSampleRate, ModulationFormat aModulationFormat, ChannelSelector aChannelSelector, ModemMode aModemModem, FrequencyThreshold aFrequencyThreshold ) {
		if ( aSampleRate.getValue() % aModemModem.getBaudRate() > 0 ) {
			throw new IllegalArgumentException( "The provided soft modem's <" + aModemModem + "> baud rate <" + aModemModem.getBaudRate() + "> does not match with the sample rate <" + aSampleRate + "> with value <" + aSampleRate.getValue() + ">." );
		}
		_sampleRate = aSampleRate;
		_modulationFormat = aModulationFormat;
		_channelSelector = aChannelSelector;
		_softModem = aModemModem;
		_higherFrequencyUpperThreshold = aModemModem.getHigherFrequency() + Math.round( ( aModemModem.getHigherFrequency() * aFrequencyThreshold.getPercent() ) / 100.0f );
		_lowerFrequencyUpperThreshold = aModemModem.getLowerFrequency() + Math.round( ( aModemModem.getLowerFrequency() * aFrequencyThreshold.getPercent() ) / 100.0f );
		_samplesPerBit = ModemMetrics.super.toSamplesPerBit();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SampleRate getSampleRate() {
		return _sampleRate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModulationFormat getModulationFormat() {
		return _modulationFormat;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChannelSelector getChannelSelector() {
		return _channelSelector;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModemMode getModemMode() {
		return _softModem;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FrequencyThreshold getFrequencyThreshold() {
		return _frequencyThreshold;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toHigherFrequencyUpperThreshold() {
		return _higherFrequencyUpperThreshold;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toLowerFrequencyUpperThreshold() {
		return _lowerFrequencyUpperThreshold;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toSamplesPerBit() {
		return _samplesPerBit;
	}
}
