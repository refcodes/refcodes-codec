// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a Demodulator-Status property.
 */
public interface DemodulatorStatusAccessor {

	/**
	 * Retrieves the Demodulator-Status from the Demodulator-Status property.
	 * 
	 * @return The Demodulator-Status stored by the Demodulator-Status property.
	 */
	DemodulatorStatus getDemodulatorStatus();

	/**
	 * Provides a mutator for a Demodulator-Status property.
	 */
	public interface DemodulatorStatusMutator {

		/**
		 * Sets the Demodulator-Status for the Demodulator-Status property.
		 * 
		 * @param aDemodulatorStatus The Demodulator-Status to be stored by the
		 *        Demodulator-Status property.
		 */
		void setDemodulatorStatus( DemodulatorStatus aDemodulatorStatus );
	}

	/**
	 * Provides a builder method for a Demodulator-Status property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DemodulatorStatusBuilder<B extends DemodulatorStatusBuilder<B>> {

		/**
		 * Sets the Demodulator-Status for the Demodulator-Status property.
		 * 
		 * @param aDemodulatorStatus The Demodulator-Status to be stored by the
		 *        Demodulator-Status property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDemodulatorStatus( DemodulatorStatus aDemodulatorStatus );
	}

	/**
	 * Provides a Demodulator-Status property.
	 */
	public interface DemodulatorStatusProperty extends DemodulatorStatusAccessor, DemodulatorStatusMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link DemodulatorStatus} (setter) as of
		 * {@link #setDemodulatorStatus(DemodulatorStatus)} and returns the very
		 * same value (getter).
		 * 
		 * @param aDemodulatorStatus The {@link DemodulatorStatus} to set (via
		 *        {@link #setDemodulatorStatus(DemodulatorStatus)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default DemodulatorStatus letDemodulatorStatus( DemodulatorStatus aDemodulatorStatus ) {
			setDemodulatorStatus( aDemodulatorStatus );
			return aDemodulatorStatus;
		}
	}
}
