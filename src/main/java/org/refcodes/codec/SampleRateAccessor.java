// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a sample rate property.
 */
public interface SampleRateAccessor {

	/**
	 * Retrieves the sample rate from the sample rate property.
	 * 
	 * @return The sample rate stored by the sample rate property.
	 */
	SampleRate getSampleRate();

	/**
	 * Provides a mutator for a sample rate property.
	 */
	public interface SampleRateMutator {

		/**
		 * Sets the sample rate for the sample rate property.
		 * 
		 * @param aSampleRate The sample rate to be stored by the sample rate
		 *        property.
		 */
		void setSampleRate( SampleRate aSampleRate );
	}

	/**
	 * Provides a builder method for a sample rate property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SampleRateBuilder<B extends SampleRateBuilder<B>> {

		/**
		 * Sets the sample rate for the sample rate property.
		 * 
		 * @param aSampleRate The sample rate to be stored by the sample rate
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSampleRate( SampleRate aSampleRate );
	}

	/**
	 * Provides a sample rate property.
	 */
	public interface SampleRateProperty extends SampleRateAccessor, SampleRateMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SampleRate}
		 * (setter) as of {@link #setSampleRate(SampleRate)} and returns the
		 * very same value (getter).
		 * 
		 * @param aSampleRate The {@link SampleRate} to set (via
		 *        {@link #setSampleRate(SampleRate)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SampleRate letSampleRate( SampleRate aSampleRate ) {
			setSampleRate( aSampleRate );
			return aSampleRate;
		}
	}
}
