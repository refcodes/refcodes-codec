package org.refcodes.codec;

/**
 * The Enum ChannelSelector.
 */
public enum ChannelSelector {

	MONO,

	STEREO
}
