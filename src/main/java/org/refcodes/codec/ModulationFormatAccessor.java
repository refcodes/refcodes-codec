// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a modulation format property.
 */
public interface ModulationFormatAccessor {

	/**
	 * Retrieves the modulation format from the pulse-code modulation format
	 * property.
	 * 
	 * @return The modulation format stored by the pulse-code modulation format
	 *         property.
	 */
	ModulationFormat getModulationFormat();

	/**
	 * Provides a mutator for a modulation format property.
	 */
	public interface ModulationFormatMutator {

		/**
		 * Sets the modulation format for the pulse-code modulation format
		 * property.
		 * 
		 * @param aModulationFormat The modulation format to be stored by the
		 *        modulation format property.
		 */
		void setModulationFormat( ModulationFormat aModulationFormat );
	}

	/**
	 * Provides a builder method for a modulation format property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ModulationFormatBuilder<B extends ModulationFormatBuilder<B>> {

		/**
		 * Sets the modulation format for the pulse-code modulation format
		 * property.
		 * 
		 * @param aModulationFormat The modulation format to be stored by the
		 *        modulation format property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withModulationFormat( ModulationFormat aModulationFormat );
	}

	/**
	 * Provides a modulation format property.
	 */
	public interface ModulationFormatProperty extends ModulationFormatAccessor, ModulationFormatMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ModulationFormat}
		 * (setter) as of {@link #setModulationFormat(ModulationFormat)} and
		 * returns the very same value (getter).
		 * 
		 * @param aModulationFormat The {@link ModulationFormat} to set (via
		 *        {@link #setModulationFormat(ModulationFormat)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ModulationFormat letModulationFormat( ModulationFormat aModulationFormat ) {
			setModulationFormat( aModulationFormat );
			return aModulationFormat;
		}
	}
}
