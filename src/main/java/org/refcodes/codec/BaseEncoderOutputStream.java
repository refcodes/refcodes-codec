// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * The {@link BaseEncoderOutputStream} is an implementations of the
 * {@link BaseEncoder} interface to be used with {@link OutputStream} instances.
 * Make sure to call {@link #close()} when done as the final padding bytes are
 * appended to the end!
 */
public class BaseEncoderOutputStream extends OutputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private BaseEncoder _encoder;

	/**
	 * Constructs a {@link BaseEncoderOutputStream} using the given
	 * {@link OutputStream} from which to read decoded data and using the
	 * {@link BaseMetrics} to encode the data.
	 * 
	 * @param aOutputStream The {@link OutputStream} from which to read decoded
	 *        data.
	 * @param aBaseMetrics The {@link BaseMetrics} to use to encode the encoded
	 *        data.
	 * 
	 * @throws IOException throw in case using the {@link OutputStream} caused
	 *         I/O related problems.
	 */
	public BaseEncoderOutputStream( OutputStream aOutputStream, BaseMetrics aBaseMetrics ) throws IOException {
		_encoder = new BaseEncoder( aOutputStream, aBaseMetrics );
	}

	/**
	 * Constructs a {@link BaseEncoderOutputStream} using the given {@link File}
	 * from which to read decoded data and using the {@link BaseMetrics} to
	 * encode the data.
	 * 
	 * @param aOutputFile The {@link File} from which to read decoded data.
	 * @param aBaseMetrics The {@link BaseMetrics} to use to encode the encoded
	 *        data.
	 * 
	 * @throws IOException throw in case using the {@link OutputStream} caused
	 *         I/O related problems.
	 */
	public BaseEncoderOutputStream( File aOutputFile, BaseMetrics aBaseMetrics ) throws IOException {
		this( new FileOutputStream( aOutputFile ), aBaseMetrics );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write( int aByte ) throws IOException {
		_encoder.transmitByte( (byte) aByte );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_encoder.flush();
		super.flush();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Make sure to call {@link #close()} when done as the final padding bytes
	 * are appended to the end!
	 */
	@Override
	public void close() throws IOException {
		_encoder.close();
		super.close();
	}
}
