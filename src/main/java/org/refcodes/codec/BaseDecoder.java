// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import org.refcodes.codec.BaseMetricsAccessor.BaseMetricsBuilder;
import org.refcodes.codec.BaseMetricsAccessor.BaseMetricsProperty;
import org.refcodes.component.AbstractConnectableAutomaton;
import org.refcodes.component.ConnectionComponent;
import org.refcodes.component.Openable;
import org.refcodes.io.BytesDestination;
import org.refcodes.io.BytesReceiver;
import org.refcodes.io.BytesReceiverDecorator;
import org.refcodes.io.InputStreamByteReceiver;

/**
 * The {@link BaseDecoder} implements the {@link BaseBuilder} functionality in
 * terms of a {@link BytesReceiver}. It implements the {@link Decoder} interface
 * attaching to the "refcodes-io" artifact via the {@link BytesReceiver}
 * interface. This increases the scope of usage scenarios.
 */
public class BaseDecoder extends AbstractConnectableAutomaton implements BaseMetricsProperty, BaseMetricsBuilder<BaseDecoder>, Decoder {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int BYTE_MASK = 0xFF;
	private static final int BITS_PER_BYTE = 8;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private BytesReceiver _byteReceiver;
	private BaseMetrics _baseMetrics = BaseMetricsConfig.BASE64;
	private int _trailingBytes = 0;
	int _word = 0;
	private byte[] _decodedBytes = new byte[_baseMetrics.getBytesPerInt()];
	private int _readIndex = 0;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link BaseDecoder} reading the data to be decoded from
	 * the provided {@link InputStream}.
	 *
	 * @param aInputStream the input stream
	 * 
	 * @throws IOException throw in case using the {@link InputStream} caused
	 *         I/O related problems.
	 */
	public BaseDecoder( InputStream aInputStream ) throws IOException {
		this( new InputStreamByteReceiver( aInputStream ) );
	}

	/**
	 * Constructs the {@link BaseDecoder} reading the data to be decoded from
	 * the provided {@link InputStream}.
	 *
	 * @param aInputStream the input stream
	 * @param aBaseMetrics The {@link BaseMetrics} to use.
	 * 
	 * @throws IOException throw in case using the {@link InputStream} caused
	 *         I/O related problems.
	 */
	public BaseDecoder( InputStream aInputStream, BaseMetrics aBaseMetrics ) throws IOException {
		this( new InputStreamByteReceiver( aInputStream ), aBaseMetrics );
	}

	/**
	 * Constructs the {@link BaseDecoder} reading the data to be decoded from
	 * the provided {@link BytesDestination}.
	 * 
	 * @param aByteProvider The {@link BytesDestination} from which to read the
	 *        data.
	 */
	public BaseDecoder( BytesDestination aByteProvider ) {
		try {
			open( new BytesReceiverDecorator( aByteProvider ) );
		}
		catch ( IOException ignore ) {}
	}

	/**
	 * Constructs the {@link BaseDecoder} reading the data to be decoded from
	 * the provided {@link BytesReceiver}.
	 * 
	 * @param aByteReceiver The {@link BytesReceiver} from which to read the
	 *        data.
	 * 
	 * @throws IOException in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	public BaseDecoder( BytesReceiver aByteReceiver ) throws IOException {
		open( aByteReceiver );
	}

	/**
	 * Constructs the {@link BaseDecoder} reading the data to be decoded from
	 * the provided {@link InputStreamByteReceiver}.
	 * 
	 * @param aInputStreamByteReceiver The {@link InputStreamByteReceiver} to
	 *        use.
	 * @param aBaseMetrics The {@link BaseMetrics} to use.
	 * 
	 * @throws IOException in case opening or accessing an open line
	 *         (connection, junction, link) caused problems.
	 */
	public BaseDecoder( InputStreamByteReceiver aInputStreamByteReceiver, BaseMetrics aBaseMetrics ) throws IOException {
		this( aInputStreamByteReceiver );
		setBaseMetrics( aBaseMetrics );
	}

	/**
	 * Explicit default constructor.
	 */
	protected BaseDecoder() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BaseMetrics getBaseMetrics() {
		return _baseMetrics;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBaseMetrics( BaseMetrics aBaseMetrics ) {
		_baseMetrics = aBaseMetrics;
		_decodedBytes = new byte[_baseMetrics.getBytesPerInt()];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BaseDecoder withBaseMetrics( BaseMetrics aBaseMetrics ) {
		setBaseMetrics( aBaseMetrics );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _byteReceiver.available();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] receiveAllBytes() throws IOException {
		if ( _readIndex > 0 ) {
			final byte[] theDecodedBytes = new byte[_decodedBytes.length - _readIndex];
			int l = 0;
			for ( int i = _readIndex; i < _decodedBytes.length; i++ ) {
				theDecodedBytes[l++] = _decodedBytes[i];
			}
			_readIndex = 0;
			return theDecodedBytes;
		}
		if ( !_byteReceiver.hasAvailable() ) {
			throw new EOFException( "Reached end of file (stream), no more data avaialble!" );
		}
		try {
			char eRead;
			for ( int l = 0; l < _baseMetrics.getDigitsPerInt(); l++ ) {
				eRead = (char) _byteReceiver.receiveByte();
				while ( eRead == '\n' || eRead == '\r' ) { // Skip any line breaks!
					eRead = (char) _byteReceiver.receiveByte();
				}
				_word <<= _baseMetrics.getBitsPerDigit();
				if ( eRead != _baseMetrics.getPaddingChar() ) {
					_word |= _baseMetrics.toValue( eRead ) & BYTE_MASK;
				}
				else {
					_trailingBytes++;
				}
			}
			for ( int i = 0; i < _baseMetrics.getBytesPerInt(); i++ ) {
				final int theIndex = _baseMetrics.getBytesPerInt() - i - 1;
				_decodedBytes[theIndex] = (byte) _word;
				_word >>= BITS_PER_BYTE;
			}
			_word = 0;
		}
		catch ( IOException e ) {
			throw new IOException( "Unable to read from the provided receiver <" + _byteReceiver + ">!", e );
		}

		if ( _trailingBytes != 0 ) {
			final int theLastBlockSize = _baseMetrics.getBytesPerInt() - _trailingBytes;
			final byte[] theDecodedBytes = new byte[theLastBlockSize];
			for ( int i = 0; i < theLastBlockSize; i++ ) {
				theDecodedBytes[i] = _decodedBytes[i];
			}
			return theDecodedBytes;
		}

		return _decodedBytes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte receiveByte() throws IOException {
		if ( _readIndex >= _baseMetrics.getBytesPerInt() - _trailingBytes ) {
			if ( !_byteReceiver.hasAvailable() ) {
				throw new EOFException( "Reached end of file (stream), no more data avaialble!" );
			}
			_trailingBytes = 0;
			_readIndex = 0;
		}
		if ( _readIndex == 0 ) {
			receiveAllBytes();
		}
		final byte theDecodedByte = _decodedBytes[_readIndex++];
		if ( _readIndex >= _decodedBytes.length ) {
			_readIndex = 0;
		}
		return theDecodedByte;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void close() throws IOException {
		try {
			_byteReceiver.close();
		}
		catch ( IOException e ) {
			throw new IOException( "Unable to close the receiver <" + _byteReceiver + ">", e );
		}
		super.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Open.
	 *
	 * @param aConnection the connection
	 * 
	 * @throws IOException the open exception
	 */
	protected void open( BytesDestination aConnection ) throws IOException {
		if ( aConnection instanceof BytesReceiver ) {
			_byteReceiver = (BytesReceiver) aConnection;
		}
		else {
			_byteReceiver = new BytesReceiverDecorator( aConnection );
		}
		if ( !_byteReceiver.isOpened() ) {
			if ( _byteReceiver instanceof Openable ) {
				( (Openable) _byteReceiver ).open();
			}
			else {
				throw new IOException( "The provided connection is in status <" + _byteReceiver.getConnectionStatus() + "> but does not provide the <" + Openable.class.getName() + "> interface." );
			}
		}
		open();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link BaseDecoderConnection} implements the {@link BaseDecoder}
	 * functionality in terms of a {@link ConnectionComponent}. In addition to
	 * the {@link BaseDecoder} it provides means to open a dedicated
	 * {@link BytesDestination} connection.
	 */
	public static class BaseDecoderConnection extends BaseDecoder implements ConnectionComponent<BytesDestination> {

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void open( BytesDestination aConnection ) throws IOException {
			super.open( aConnection );
		}
	}
}
