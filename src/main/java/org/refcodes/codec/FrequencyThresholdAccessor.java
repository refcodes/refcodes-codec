// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a frequency threshold property.
 */
public interface FrequencyThresholdAccessor {

	/**
	 * Retrieves the frequency threshold from the frequency threshold property.
	 * 
	 * @return The frequency threshold stored by the frequency threshold
	 *         property.
	 */
	FrequencyThreshold getFrequencyThreshold();

	/**
	 * Provides a mutator for a frequency threshold property.
	 */
	public interface FrequencyThresholdMutator {

		/**
		 * Sets the frequency threshold for the frequency threshold property.
		 * 
		 * @param aFrequencyThreshold The frequency threshold to be stored by
		 *        the frequency threshold property.
		 */
		void setFrequencyThreshold( FrequencyThreshold aFrequencyThreshold );
	}

	/**
	 * Provides a builder method for a frequency threshold property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FrequencyThresholdBuilder<B extends FrequencyThresholdBuilder<B>> {

		/**
		 * Sets the frequency threshold for the frequency threshold property.
		 * 
		 * @param aFrequencyThreshold The frequency threshold to be stored by
		 *        the frequency threshold property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFrequencyThreshold( FrequencyThreshold aFrequencyThreshold );
	}

	/**
	 * Provides a frequency threshold property.
	 */
	public interface FrequencyThresholdProperty extends FrequencyThresholdAccessor, FrequencyThresholdMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link FrequencyThreshold} (setter) as of
		 * {@link #setFrequencyThreshold(FrequencyThreshold)} and returns the
		 * very same value (getter).
		 * 
		 * @param aFrequencyThreshold The {@link FrequencyThreshold} to set (via
		 *        {@link #setFrequencyThreshold(FrequencyThreshold)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default FrequencyThreshold letFrequencyThreshold( FrequencyThreshold aFrequencyThreshold ) {
			setFrequencyThreshold( aFrequencyThreshold );
			return aFrequencyThreshold;
		}
	}
}
