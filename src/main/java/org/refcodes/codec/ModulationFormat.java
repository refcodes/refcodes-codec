// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.codec;

/**
 * Pulse-code modulation format: Defines the according pulse-code modulation
 * format with the according silence threshold (root mean square).
 */
public enum ModulationFormat {

	BYTE(1, 7),

	SHORT(2, 2000);

	private int _bytesPerSample;

	private int _silenceThreshold;

	/**
	 * Instantiates a new modulation format.
	 *
	 * @param aBytesPerSample the bytes per sample
	 * @param aSilenceThreshold the silence threshold
	 */
	private ModulationFormat( int aBytesPerSample, int aSilenceThreshold ) {
		_bytesPerSample = aBytesPerSample;
		_silenceThreshold = aSilenceThreshold;
	}

	/**
	 * Returns the bytes/sample for the according audio mode.
	 * 
	 * @return The according bytes/sample.
	 */
	public int getBytesPerSample() {
		return _bytesPerSample;
	}

	/**
	 * Returns the root mean square silence threshold for the according audio
	 * mode.
	 * 
	 * @return The according silence threshold.
	 */
	public int getSilenceThreshold() {
		return _silenceThreshold;
	}
}
