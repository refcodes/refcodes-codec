// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a Modem-Mode property.
 */
public interface ModemModeAccessor {

	/**
	 * Retrieves the Modem-Mode from the Modem-Mode property.
	 * 
	 * @return The Modem-Mode stored by the Modem-Mode property.
	 */
	ModemMode getModemMode();

	/**
	 * Provides a mutator for a Modem-Mode property.
	 */
	public interface ModemModeMutator {

		/**
		 * Sets the Modem-Mode for the Modem-Mode property.
		 * 
		 * @param aModemMode The Modem-Mode to be stored by the Modem-Mode
		 *        property.
		 */
		void setModemMode( ModemMode aModemMode );
	}

	/**
	 * Provides a builder method for a Modem-Mode property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ModemModeBuilder<B extends ModemModeBuilder<B>> {

		/**
		 * Sets the Modem-Mode for the Modem-Mode property.
		 * 
		 * @param aModemMode The Modem-Mode to be stored by the Modem-Mode
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withModemMode( ModemMode aModemMode );
	}

	/**
	 * Provides a Modem-Mode property.
	 */
	public interface ModemModeProperty extends ModemModeAccessor, ModemModeMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ModemMode}
		 * (setter) as of {@link #setModemMode(ModemMode)} and returns the very
		 * same value (getter).
		 * 
		 * @param aModemMode The {@link ModemMode} to set (via
		 *        {@link #setModemMode(ModemMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ModemMode letModemMode( ModemMode aModemMode ) {
			setModemMode( aModemMode );
			return aModemMode;
		}
	}
}
