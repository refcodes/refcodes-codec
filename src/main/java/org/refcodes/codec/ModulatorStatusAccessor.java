// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a Modulator-Status property.
 */
public interface ModulatorStatusAccessor {

	/**
	 * Retrieves the Modulator-Status from the Modulator-Status property.
	 * 
	 * @return The Modulator-Status stored by the Modulator-Status property.
	 */
	ModulatorStatus getModulatorStatus();

	/**
	 * Provides a mutator for a Modulator-Status property.
	 */
	public interface ModulatorStatusMutator {

		/**
		 * Sets the Modulator-Status for the Modulator-Status property.
		 * 
		 * @param aModulatorStatus The Modulator-Status to be stored by the
		 *        Modulator-Status property.
		 */
		void setModulatorStatus( ModulatorStatus aModulatorStatus );
	}

	/**
	 * Provides a builder method for a Modulator-Status property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ModulatorStatusBuilder<B extends ModulatorStatusBuilder<B>> {

		/**
		 * Sets the Modulator-Status for the Modulator-Status property.
		 * 
		 * @param aModulatorStatus The Modulator-Status to be stored by the
		 *        Modulator-Status property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withModulatorStatus( ModulatorStatus aModulatorStatus );
	}

	/**
	 * Provides a Modulator-Status property.
	 */
	public interface ModulatorStatusProperty extends ModulatorStatusAccessor, ModulatorStatusMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ModulatorStatus}
		 * (setter) as of {@link #setModulatorStatus(ModulatorStatus)} and
		 * returns the very same value (getter).
		 * 
		 * @param aModulatorStatus The {@link ModulatorStatus} to set (via
		 *        {@link #setModulatorStatus(ModulatorStatus)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ModulatorStatus letModulatorStatus( ModulatorStatus aModulatorStatus ) {
			setModulatorStatus( aModulatorStatus );
			return aModulatorStatus;
		}
	}
}
