// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import org.refcodes.component.ConnectionComponent;
import org.refcodes.io.BytesSource;

/**
 * The {@link ModemEncoder} implements a Frequency-shift keyring encoder. "...
 * Frequency-shift keyring (FSK) is a frequency modulation scheme in which
 * digital information is transmitted through discrete frequency changes of a
 * carrier signal ..." (Wikipedia)
 * 
 * @see "https://en.wikipedia.org/wiki/Frequency-shift_keyring"
 */
public interface ModemEncoder extends Encoder, ModulatorStatusAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link ModemEncoderConsumer} implements the {@link ModemEncoder}
	 * functionality in terms of a {@link ConnectionComponent}. In addition to
	 * the {@link ModemEncoder} it provides means to open a dedicated
	 * {@link BytesSource} connection.
	 */
	public interface ModemEncoderConsumer extends ModemEncoder, ConnectionComponent<BytesSource> {}

}
