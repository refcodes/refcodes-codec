// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.codec;

import org.refcodes.mixin.ModeAccessor;

/**
 * The Enum ModemMode.
 */
public enum ModemMode implements ModeAccessor<Integer> {

	// @formatter:off
	SOFT_MODEM_1(1, 126, 882, 1764), 
	
	SOFT_MODEM_2(2, 315,1575, 3150), 
	
	SOFT_MODEM_3(3, 630, 3150, 6300), 
	
	SOFT_MODEM_4(4, 1225, 4900, 7350), 
	
	SOFT_MODEM_5(5, 2450, 4900, 7350);
	// @formatter:on

	private int _mode;

	private int _baudRate;

	private int _lowerFrequency;

	private int _higherFrequency;

	/**
	 * Instantiates a new modem mode.
	 *
	 * @param aMode the mode
	 * @param aBaudRate the baud rate
	 * @param aLowerFrequency the lower frequency
	 * @param aHigherFrequency the higher frequency
	 */
	private ModemMode( int aMode, int aBaudRate, int aLowerFrequency, int aHigherFrequency ) {
		_mode = aMode;
		_baudRate = aBaudRate;
		_lowerFrequency = aLowerFrequency;
		_higherFrequency = aHigherFrequency;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getMode() {
		return _mode;
	}

	/**
	 * The baud rate of this this mode.
	 * 
	 * @return The baud rate.
	 */
	public int getBaudRate() {
		return _baudRate;
	}

	/**
	 * The lower frequency for this mode.
	 * 
	 * @return The lower frequency.
	 */
	public int getLowerFrequency() {
		return _lowerFrequency;
	}

	/**
	 * The higher frequency for this mode.
	 * 
	 * @return The higher frequency.
	 */
	public int getHigherFrequency() {
		return _higherFrequency;
	}
}
