// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a base codec metrics property.
 */
public interface BaseMetricsAccessor {

	/**
	 * Retrieves the base codec metrics from the base codec metrics property.
	 * 
	 * @return The base codec metrics stored by the base codec metrics property.
	 */
	BaseMetrics getBaseMetrics();

	/**
	 * Provides a mutator for a base codec metrics property.
	 */
	public interface BaseMetricsMutator {

		/**
		 * Sets the base codec metrics for the base codec metrics property.
		 * 
		 * @param aBaseMetrics The base codec metrics to be stored by the font
		 *        style property.
		 */
		void setBaseMetrics( BaseMetrics aBaseMetrics );
	}

	/**
	 * Provides a builder method for a base codec metrics property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BaseMetricsBuilder<B extends BaseMetricsBuilder<B>> {

		/**
		 * Sets the base codec metrics for the base codec metrics property.
		 * 
		 * @param aBaseMetrics The base codec metrics to be stored by the font
		 *        style property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBaseMetrics( BaseMetrics aBaseMetrics );
	}

	/**
	 * Provides a base codec metrics property.
	 */
	public interface BaseMetricsProperty extends BaseMetricsAccessor, BaseMetricsMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link BaseMetrics}
		 * (setter) as of {@link #setBaseMetrics(BaseMetrics)} and returns the
		 * very same value (getter).
		 * 
		 * @param aBaseMetrics The {@link BaseMetrics} to set (via
		 *        {@link #setBaseMetrics(BaseMetrics)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default BaseMetrics letBaseMetrics( BaseMetrics aBaseMetrics ) {
			setBaseMetrics( aBaseMetrics );
			return aBaseMetrics;
		}
	}
}
