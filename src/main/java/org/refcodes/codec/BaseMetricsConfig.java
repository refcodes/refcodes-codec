// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import org.refcodes.data.CharSet;
import org.refcodes.data.PaddingChar;

/**
 * This enumeration provides access to preconfigured {@link BaseMetrics} to be
 * used by a {@link BaseBuilder}, a {@link BaseDecoder}, a {@link BaseEncoder}
 * or the like..
 */
public enum BaseMetricsConfig implements BaseMetrics {

	// @formatter:off
	BASE2(2, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	BINARY(2, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	BASE4(4, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	BASE8(8, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	OCTAL(8, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	/**
	 * Use this encoding when you want to retrieve a "valid" number *not*
	 * beginning with a zero "0" and just consisting of the numbers 0 to 9. E.g.
	 * when a "number" variable is to be encoded and still must look like a
	 * number without any prefixed "0" chars. "0" is used as padding char being 
	 * suffixed.
	 */
	ENCODED_AS_NUMBER(8, CharSet.ENCODED_AS_NUMBER.getCharSet(), '0' ),
	BASE16(16, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	HEXADECIMAL(16, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	BASE32(32, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	BASE64(64, CharSet.BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	BASE64_ARABIC(64, CharSet.ARABIC_BASE64.getCharSet(), PaddingChar.BASE64.getChar() ),
	BASE64_URL(64, CharSet.BASE64URL.getCharSet(), PaddingChar.BASE64.getChar() );
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private BaseMetrics _baseMetrics;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new base config.
	 *
	 * @param aBase the base
	 * @param aCharSet the char set
	 * @param aPaddingChar the padding char
	 */
	private BaseMetricsConfig( int aBase, char[] aCharSet, char aPaddingChar ) {
		_baseMetrics = new BaseMetricsImpl( aBase, aCharSet, aPaddingChar );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getNumberBase() {
		return _baseMetrics.getNumberBase();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] getCharSet() {
		return _baseMetrics.getCharSet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBytesPerInt() {
		return _baseMetrics.getBytesPerInt();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getDigitsPerInt() {
		return _baseMetrics.getDigitsPerInt();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBitsPerDigit() {
		return _baseMetrics.getBitsPerDigit();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getDigitsPerByte() {
		return _baseMetrics.getDigitsPerByte();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getDigitMask() {
		return _baseMetrics.getDigitMask();
	}

	// /////////////////////////////////////////////////////////////////////////
	// UTILITY:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To base codec.
	 *
	 * @param aNumberBase the number base
	 * 
	 * @return the base metrics
	 */
	public static BaseMetrics toBaseCodec( int aNumberBase ) {
		for ( BaseMetrics aBaseCodec : values() ) {
			if ( aBaseCodec.getNumberBase() == aNumberBase ) {
				return aBaseCodec;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int toValue( char aChar ) {
		return _baseMetrics.toValue( aChar );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char toChar( int aValue ) {
		return _baseMetrics.toChar( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getPaddingChar() {
		return _baseMetrics.getPaddingChar();
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public String toString() {
	//		return _baseMetrics.toString();
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBytesPerBlock() {
		return _baseMetrics.getBytesPerBlock();
	}
}
