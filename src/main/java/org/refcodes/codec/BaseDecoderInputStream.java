// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.io.FilterInputStream;

/**
 * {@link InputStream} decoding data being read.
 */
public class BaseDecoderInputStream extends InputStream {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final char[] WHITESPACES = new char[] { '\n', '\r', '\f', '\t', ' ' };

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private BaseDecoder _decoder;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link BaseDecoderInputStream} using the given
	 * {@link InputStream} from which to read encoded data and using the
	 * {@link BaseMetrics} to decode the data.
	 * 
	 * @param aInputStream The {@link InputStream} from which to read encoded
	 *        data.
	 * @param aBaseMetrics The {@link BaseMetrics} to use to decode the encoded
	 *        data.
	 * 
	 * @throws IOException throw in case using the {@link InputStream} caused
	 *         I/O related problems.
	 */
	public BaseDecoderInputStream( InputStream aInputStream, BaseMetrics aBaseMetrics ) throws IOException {
		_decoder = new BaseDecoder( aInputStream, aBaseMetrics );
	}

	/**
	 * Constructs a {@link BaseDecoderInputStream} using the given
	 * {@link InputStream} from which to read encoded data and using the
	 * {@link BaseMetrics} to decode the data.
	 * 
	 * @param aInputStream The {@link InputStream} from which to read encoded
	 *        data.
	 * @param aBaseMetrics The {@link BaseMetrics} to use to decode the encoded
	 *        data.
	 * 
	 * @param isFilterWhiteSpaces When true, then all white spaces not found in
	 *        the {@link BaseMetrics#getCharSet()} are filtered from the
	 *        {@link InputStream}.
	 * 
	 * @throws IOException throw in case using the {@link InputStream} caused
	 *         I/O related problems.
	 */
	public BaseDecoderInputStream( InputStream aInputStream, BaseMetrics aBaseMetrics, boolean isFilterWhiteSpaces ) throws IOException {
		final List<Character> theComplement = new ArrayList<>();
		final char[] theCharSet = aBaseMetrics.getCharSet();
		for ( char aWHITESPACES : WHITESPACES ) {
			out: {
				for ( char aTheCharSet : theCharSet ) {
					if ( aTheCharSet == aWHITESPACES ) {
						break out;
					}
				}
				theComplement.add( aWHITESPACES );
			}
		}
		if ( theComplement.size() > 0 ) {
			final char[] theFilterChars = new char[theComplement.size()];
			for ( int i = 0; i < theFilterChars.length; i++ ) {
				theFilterChars[i] = theComplement.get( i );
			}
			aInputStream = new FilterInputStream( aInputStream, theFilterChars );
		}
		_decoder = new BaseDecoder( aInputStream, aBaseMetrics );
	}

	/**
	 * Constructs a {@link BaseDecoderInputStream} using the given {@link File}
	 * from which to read encoded data and using the {@link BaseMetrics} to
	 * decode the data.
	 * 
	 * @param aInputFile The {@link File} from which to read encoded data.
	 * @param aBaseMetrics The {@link BaseMetrics} to use to decode the encoded
	 *        data.
	 * 
	 * @throws IOException throw in case using the {@link InputStream} caused
	 *         I/O related problems.
	 */
	public BaseDecoderInputStream( File aInputFile, BaseMetrics aBaseMetrics ) throws IOException {
		this( new FileInputStream( aInputFile ), aBaseMetrics );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int read() throws IOException {
		try {
			return Byte.toUnsignedInt( _decoder.receiveByte() );
		}
		catch ( EOFException e ) {
			return -1;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_decoder.close();
		super.close();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int available() throws IOException {
		return _decoder.available();
	}
}
