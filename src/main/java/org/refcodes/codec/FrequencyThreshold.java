package org.refcodes.codec;

import org.refcodes.mixin.PercentAccessor;

/**
 * The Enum FrequencyThreshold.
 */
public enum FrequencyThreshold implements PercentAccessor {

	THRESHOLD_1(1), THRESHOLD_5(5), THRESHOLD_10(10), THRESHOLD_20(20);

	private int _percent;

	/**
	 * Instantiates a new frequency threshold.
	 *
	 * @param aThresholdPercent the threshold percent
	 */
	private FrequencyThreshold( int aThresholdPercent ) {
		_percent = aThresholdPercent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPercent() {
		return _percent;
	}
}
