// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

/**
 * Provides an accessor for a channel selector property.
 */
public interface ChannelSelectorAccessor {

	/**
	 * Retrieves the channel selector from the channel selector property.
	 * 
	 * @return The channel selector stored by the channel selector property.
	 */
	ChannelSelector getChannelSelector();

	/**
	 * Provides a mutator for a channel selector property.
	 */
	public interface ChannelSelectorMutator {

		/**
		 * Sets the channel selector for the channel selector property.
		 * 
		 * @param aChannelSelector The channel selector to be stored by the
		 *        channel selector property.
		 */
		void setChannelSelector( ChannelSelector aChannelSelector );
	}

	/**
	 * Provides a builder method for a channel selector property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ChannelSelectorBuilder<B extends ChannelSelectorBuilder<B>> {

		/**
		 * Sets the channel selector for the channel selector property.
		 * 
		 * @param aChannelSelector The channel selector to be stored by the
		 *        channel selector property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withChannelSelector( ChannelSelector aChannelSelector );
	}

	/**
	 * Provides a channel selector property.
	 */
	public interface ChannelSelectorProperty extends ChannelSelectorAccessor, ChannelSelectorMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ChannelSelector}
		 * (setter) as of {@link #setChannelSelector(ChannelSelector)} and
		 * returns the very same value (getter).
		 * 
		 * @param aChannelSelector The {@link ChannelSelector} to set (via
		 *        {@link #setChannelSelector(ChannelSelector)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ChannelSelector letChannelSelector( ChannelSelector aChannelSelector ) {
			setChannelSelector( aChannelSelector );
			return aChannelSelector;
		}
	}
}
