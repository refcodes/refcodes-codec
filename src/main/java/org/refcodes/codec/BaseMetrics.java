// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import org.refcodes.mixin.CharSetAccessor;
import org.refcodes.numerical.NumberBaseAccessor;
import org.refcodes.numerical.PaddingCharAccessor;
import org.refcodes.textual.TextLineBuilder;

/**
 * The {@link BaseMetrics} provide the metrics for a codec to be used by an
 * implementation of the {@link BaseBuilder}, the {@link BaseDecoder}, the
 * {@link BaseEncoder} or the like.
 */
public interface BaseMetrics extends CharSetAccessor, PaddingCharAccessor, NumberBaseAccessor {

	/**
	 * {@inheritDoc}
	 */
	@Override
	char[] getCharSet();

	/**
	 * Number of bytes to exactly store a minimum number of digits.
	 *
	 * @return the bytes per int
	 */
	int getBytesPerInt();

	/**
	 * Number of bytes to exactly fit into an encoded block.
	 *
	 * @return the bytes per block
	 */
	int getBytesPerBlock();

	/**
	 * Number of digits stored in an integer.
	 *
	 * @return the digits per int
	 */
	int getDigitsPerInt();

	/**
	 * The size of a digits-block, any {@link String} of digits not being a
	 * multiple of the digits-block size requires padding chars.
	 *
	 * @return the digits per block.
	 */
	default int getDigitsPerBlock() {
		return getDigitsPerInt();
	}

	/**
	 * Number in bits for one digit.
	 *
	 * @return the bits per digit
	 */
	int getBitsPerDigit();

	/**
	 * Number of digits required to represent a byte.
	 *
	 * @return the digits per byte
	 */
	int getDigitsPerByte();

	/**
	 * The digit mask is the bit-field covering just the digit's bits (starting
	 * at bit 0). Those bits in the mask are set to one which are required to
	 * represent a digit.
	 *
	 * @return the digit mask
	 */
	int getDigitMask();

	/**
	 * Retrieves the (decoded) value for the (encoded) character.
	 *
	 * @param aChar the char
	 * 
	 * @return the int
	 */
	int toValue( char aChar );

	/**
	 * Retrieves the (encoded) character for the (decoded) value.
	 *
	 * @param aValue the value
	 * 
	 * @return the char
	 */
	char toChar( int aValue );

	/**
	 * Determines the number of padding characters to be appended to data of a
	 * given number of bytes to be encoded.
	 * 
	 * @param aSize The size for which to determine the number of padding
	 *        characters.
	 * 
	 * @return The according number of padding characters.
	 */
	default int toPaddingCharsNumber( int aSize ) {
		final int theMod = getBytesPerInt() - ( aSize % getBytesPerInt() );
		return theMod == getBytesPerInt() ? 0 : theMod;
	}

	/**
	 * Determines the padding {@link String} to be appended to data of the given
	 * number of bytes to be encoded.
	 * 
	 * @param aSize The size for which to determine the number of padding
	 *        characters.
	 * 
	 * @return The according padding {@link String}.
	 */
	default String toPaddingChars( int aSize ) {
		return TextLineBuilder.asString( toPaddingCharsNumber( aSize ), getPaddingChar() );
	}
}