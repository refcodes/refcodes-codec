// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.codec;

import org.refcodes.mixin.ValueAccessor;

/**
 * The Enum SampleRate.
 */
public enum SampleRate implements ValueAccessor<Integer> {

	/**
	 * {@link #STEREO} / 3 x 2 = {@link #SEMI}: In digital audio 44100 Hz is a
	 * common sampling frequency, double of #MONO.
	 */
	STEREO(44100),

	/**
	 * {@link #MONO} * 2 = {@link #STEREO}: Least common multiple for 126, 315,
	 * 630, 1225, 2450.
	 */
	MONO(22050),

	/**
	 * {@link #SEMI} / 2 x 3 = {@link #STEREO}: Least common multiple for 24,
	 * 49, 50. If we have 245 usable lines per frame that can store data, we
	 * have 245 lines/frame * 60 frames/second = 14700 lines/second. If we store
	 * an integer number of sample pairs (one left and one right) on a line then
	 * we get possible sample rates of 14700 Hz, 29400 Hz, 44100 Hz, and 58800
	 * Hz, etc.
	 */
	SEMI(29400);

	private int _sampleRate;

	/**
	 * Instantiates a new sample rate.
	 *
	 * @param aSampleRate the sample rate
	 */
	private SampleRate( int aSampleRate ) {
		_sampleRate = aSampleRate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getValue() {
		return _sampleRate;
	}
}
